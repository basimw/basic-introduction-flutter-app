import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Intro',
      theme: ThemeData(
        primaryColor: Colors.blue.shade900,
      ),
      //home: MyHomePage(title: 'My First Flutter Application'),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Introduction Basic App'),
        ),
        backgroundColor: Colors.blue.shade300,
        body: Container(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 50,
              backgroundImage: AssetImage("images/basim.jpg"),
            ),
            Text(
              "Basim Wangde",
              style: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 40, letterSpacing: 2),
            ),
            Text(
              "Software Developer",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 25, horizontal: 25),
              child: ListTile(
                leading: Icon(
                  Icons.phone,
                  color: Colors.blueAccent,
                ),
                title: Text(
                  "7977747585",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.blueAccent),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 0, horizontal: 25),
              child: ListTile(
                leading: Icon(
                  Icons.email,
                  color: Colors.blueAccent,
                ),
                title: Text(
                  "basimwangde@gmail.com",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.blueAccent),
                ),
              ),
            )
          ],
        )),
      ),
    );
  }
}
